# MUSCL Scheme
Monotonic Upwind Scheme for Conservation Laws (firstly introduced by van Leer,
1979)

## Description
Consider the convection diffusion equation,

```math
\frac{\text{d} u}{\text{d} t} + U \frac{\text{d} u}{\text{d} x} = 
\kappa \frac{\text{d}^2 u}{\text{d} x^2} + \text{F}
```

Here the second term of the l.h.s is the advection or convection term and it
advects the solution in the direction of $`U`$. First term of the r.h.s is the
diffusion term and the last term is the source term.
